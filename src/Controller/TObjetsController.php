<?php

namespace App\Controller;

use App\Entity\TObjets;
use App\Form\TObjetsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/objets")
 */
class TObjetsController extends AbstractController
{
    /**
     * @Route("/", name="t_objets_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tObjets = $entityManager
            ->getRepository(TObjets::class)
            ->findAll();

        return $this->render('t_objets/index.html.twig', [
            't_objets' => $tObjets,
        ]);
    }

    /**
     * @Route("/new", name="t_objets_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tObjet = new TObjets();
        $form = $this->createForm(TObjetsType::class, $tObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tObjet);
            $entityManager->flush();

            return $this->redirectToRoute('t_objets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_objets/new.html.twig', [
            't_objet' => $tObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_objets_show", methods={"GET"})
     */
    public function show(TObjets $tObjet): Response
    {
        return $this->render('t_objets/show.html.twig', [
            't_objet' => $tObjet,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="t_objets_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TObjets $tObjet, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TObjetsType::class, $tObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_objets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_objets/edit.html.twig', [
            't_objet' => $tObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_objets_delete", methods={"POST"})
     */
    public function delete(Request $request, TObjets $tObjet, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tObjet->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tObjet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_objets_index', [], Response::HTTP_SEE_OTHER);
    }
}
