<?php

namespace App\Controller;

use App\Entity\TCaracteristiquesObjets;
use App\Form\TCaracteristiquesObjetsType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/t/caracteristiques/objets")
 */
class TCaracteristiquesObjetsController extends AbstractController
{
    /**
     * @Route("/", name="t_caracteristiques_objets_index", methods={"GET"})
     */
    public function index(EntityManagerInterface $entityManager): Response
    {
        $tCaracteristiquesObjets = $entityManager
            ->getRepository(TCaracteristiquesObjets::class)
            ->findAll();

        return $this->render('t_caracteristiques_objets/index.html.twig', [
            't_caracteristiques_objets' => $tCaracteristiquesObjets,
        ]);
    }

    /**
     * @Route("/new", name="t_caracteristiques_objets_new", methods={"GET", "POST"})
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $tCaracteristiquesObjet = new TCaracteristiquesObjets();
        $form = $this->createForm(TCaracteristiquesObjetsType::class, $tCaracteristiquesObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($tCaracteristiquesObjet);
            $entityManager->flush();

            return $this->redirectToRoute('t_caracteristiques_objets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_caracteristiques_objets/new.html.twig', [
            't_caracteristiques_objet' => $tCaracteristiquesObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_caracteristiques_objets_show", methods={"GET"})
     */
    public function show(TCaracteristiquesObjets $tCaracteristiquesObjet): Response
    {
        return $this->render('t_caracteristiques_objets/show.html.twig', [
            't_caracteristiques_objet' => $tCaracteristiquesObjet,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="t_caracteristiques_objets_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TCaracteristiquesObjets $tCaracteristiquesObjet, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TCaracteristiquesObjetsType::class, $tCaracteristiquesObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            return $this->redirectToRoute('t_caracteristiques_objets_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('t_caracteristiques_objets/edit.html.twig', [
            't_caracteristiques_objet' => $tCaracteristiquesObjet,
            'form' => $form,
        ]);
    }

    /**
     * @Route("/{id}", name="t_caracteristiques_objets_delete", methods={"POST"})
     */
    public function delete(Request $request, TCaracteristiquesObjets $tCaracteristiquesObjet, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tCaracteristiquesObjet->getId(), $request->request->get('_token'))) {
            $entityManager->remove($tCaracteristiquesObjet);
            $entityManager->flush();
        }

        return $this->redirectToRoute('t_caracteristiques_objets_index', [], Response::HTTP_SEE_OTHER);
    }
}
