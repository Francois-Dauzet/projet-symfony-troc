<?php

namespace App\Entity;

use App\Entity\TKeys;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCaracteristiquesObjets
 * @ORM\Entity
 * @ORM\Table(
 *      name="t_caracteristiques_objets", 
 *      indexes={
 *      @ORM\Index(
 *          name="fk_object_id_idx", 
 *          columns={"fk_object"}
 *      )}
 * )
 */
class TCaracteristiquesObjets
{
    /**
     * @var int
     *
     * @ORM\Column(
     *      name="id", 
     *      type="integer", 
     *      nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(
     *      strategy="IDENTITY"
     * )
     * @Assert\GreaterThan(0)
     */
    private ?int $id = null;

    /**
     * @var int
     *
     * @ORM\Column(
     *      name="fk_key", 
     *      type="integer", 
     *      nullable=false
     * )
     */
    private ?int $fkKey = null;

    /**
     * @var int
     *
     * @ORM\Column(
     *      name="value", 
     *      type="integer", 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 0"
     *      }
     * )
     * @Assert\GreaterThan(0)
     */
    private ?int $value = null;

    /**
     * @var \TKeys
     *
     * @ORM\ManyToOne(
     *      targetEntity="TKeys"
     * )
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(
     *          name="fk_object", 
     *          referencedColumnName="id"
     *      )
     * })
     */
    private ?int $fkObject = null;

    //// Getters / Setters
    public function getId(): ?int
    {

        try {
            return $this->id;
        } catch (\Throwable $error) {
            throw $error;
        }
    }

    public function getFkKey(): ?int
    {
        return $this->fkKey;
    }

    public function setFkKey(int $fkKey): self
    {
        $this->fkKey = $fkKey;

        return $this;
    }

    public function getValue(): ?int
    {
        return $this->value;
    }

    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getFkObject(): ?TKeys
    {
        return $this->fkObject;
    }

    public function setFkObject(?TKeys $fkObject): self
    {
        $this->fkObject = $fkObject;

        return $this;
    }
}
