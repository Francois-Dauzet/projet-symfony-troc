<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCompteUtilisateur
 *
 * @ORM\Table(name="t_compte_utilisateur")
 * @ORM\Entity
 */
class TCompteUtilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(
     *      name="id", 
     *      type="integer", 
     *      nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(
     *      strategy="IDENTITY"
     * )
     * @Assert\GreaterThan(0)
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="login", 
     *      type="string", 
     *      length=45, 
     *      nullable=false, 
     *      options={
     *          "comment"="> 7 chars"
     *      }
     * )
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * 
     * @Assert\Regex(
     * pattern="/^[a-z0-9_-]{3,15}$/",
     * message="not_valid_login"
     * )
     */
    private string $login = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="password", 
     *      type="string", 
     *      length=45, 
     *      nullable=false, 
     *      options={
     *          "comment"="> 7 chars AND regex(pwd)"
     *      }
     * )
     * 
     * @Assert\Length(
     *      min = 8,
     *      max = 45,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Regex(
     * pattern="/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!.@$ %^&*-]).{8,}$/",
     * message="not_valid_password"
     * )
     */
    private string $password = '';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = filter_var($login, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $this;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function __toString()
    {
        return strval($this->login);
    }
}
