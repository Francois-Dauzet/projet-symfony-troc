<?php

//// Typage des entitées

namespace App\Entity;

use App\Entity\TObjets;
use App\Entity\TStatut;
use App\Entity\TUtilisateurs;
//// Ajouter les Constraints (Assert)
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

//// Bien indentait les annotations
/**
 * TMessage
 *
 * @ORM\Table(name="t_message", indexes={
 *      @ORM\Index(
 *          name="fk_recepteur_idx", 
 *          columns={"fk_recepteur"}
 *      ), 
 *      @ORM\Index(
 *          name="fk_emetteur_idx", 
 *          columns={"fk_emetteur"}
 *      ),
 *      @ORM\Index(
 *          name="fk_status_idx", 
 *          columns={"fk_statut"}
 *      )
 * })
 * @ORM\Entity
 */
class TMessage
{
    /**
     * // Déclacaration documentaire pour indiquer le type de la variable
     * @var int
     *
     * @ORM\Column(
     *      name="id", 
     *      type="integer", 
     *      nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(
     *      strategy="IDENTITY"
     * )
     * @Assert\GreaterThan(0)
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="objet", 
     *      type="string", 
     *      length=255, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 1"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 50,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private string $objet = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="text", 
     *      type="text", 
     *      length=65535, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 1"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 65535,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private string $text = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *      name="date", 
     *      type="datetime", 
     *      nullable=false, 
     *      options={
     *          "default"="CURRENT_TIMESTAMP",
     *          "comment"="default_value = now"
     *      }
     * )
     * @Assert\Type(type="\DateTime")
     */
    private ?\DateTime $date;

    /**
     * @var \TUtilisateurs
     *
     * @ORM\ManyToOne(
     *      targetEntity="TUtilisateurs"
     * )
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(
     *          name="fk_emetteur", 
     *          referencedColumnName="user_id"
     *      )
     * })
     */
    private $fkEmetteur;

    /**
     * @var string
     *
     * @ORM\Column(
     *  name="piece_jointe",
     *  type="string",
     *  length=250,
     *  nullable=false,
     *  options={"comment"="Length > 1"}
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 250,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * @Assert\Url
     */
    private string $pieceJointe = '';

    /** 
     * @var \File
     * 
     * @Assert\File(
     *     maxSize = "5000k",
     *     mimeTypes = {"application/pdf", "application/x-pdf"},
     *     mimeTypesMessage = "Please upload a valid PDF"
     * )
     */
    private $fichier;

    /**
     * @var \TStatut
     *
     * @ORM\ManyToOne(
     *      targetEntity="TStatut"
     * )
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(
     *          name="fk_statut", 
     *          referencedColumnName="id"
     *      )
     * })
     * @Assert\Type("App\Entity\TStatut")
     */
    private  $fkStatut;

    /**
     * @var \TUtilisateurs
     *
     * @ORM\ManyToOne(
     *      targetEntity="TUtilisateurs"
     * )
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(
     *          name="fk_recepteur", 
     *          referencedColumnName="user_id"
     *      )
     * })
     * 
     * @Assert\Type("App\Entity\TUtilisateurs")
     * 
     */
    private $fkRecepteur;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(
     *      targetEntity="TObjets", 
     *      mappedBy="fkIdMessage"
     * )
     * 
     * @Assert\Type("\Doctrine\Common\Collections\Collection")
     */
    private $fkIdObject;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date = new \DateTime();
        $this->fkIdObject = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObjet(): string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = filter_var($text, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getFkEmetteur(): TUtilisateurs
    {
        return $this->fkEmetteur;
    }

    public function setFkEmetteur(TUtilisateurs $fkEmetteur): self
    {
        $this->fkEmetteur = $fkEmetteur;

        return $this;
    }

    public function getFkStatut(): TStatut
    {
        return $this->fkStatut;
    }

    public function setFkStatut(TStatut $fkStatut): self
    {
        $this->fkStatut = $fkStatut;

        return $this;
    }

    public function getFkRecepteur(): TUtilisateurs
    {
        return $this->fkRecepteur;
    }

    public function setFkRecepteur(TUtilisateurs $fkRecepteur): self
    {
        $this->fkRecepteur = $fkRecepteur;

        return $this;
    }

    public function getPieceJointe(): string
    {
        return $this->pieceJointe;
    }

    public function setPieceJointe(string $pieceJointe): self
    {
        $this->pieceJointe = $pieceJointe;

        return $this;
    }

    // public function getFichier(): \File
    // {
    //     return $this->fichier;
    // }

    // public function setFichier(\File $fichier): self
    // {
    //     $this->fichier = $fichier;

    //     return $this;
    // }

    /**
     * @return Collection|TObjets[]
     */
    public function getFkIdObject(): Collection
    {
        return $this->fkIdObject;
    }

    public function addFkIdObject(TObjets $fkIdObject): self
    {
        if (!$this->fkIdObject->contains($fkIdObject)) {
            $this->fkIdObject[] = $fkIdObject;
            $fkIdObject->addFkIdMessage($this);
        }

        return $this;
    }

    public function removeFkIdObject(TObjets $fkIdObject): self
    {
        if ($this->fkIdObject->removeElement($fkIdObject)) {
            $fkIdObject->removeFkIdMessage($this);
        }

        return $this;
    }
}
