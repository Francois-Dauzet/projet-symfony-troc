<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TCpVille
 *
 * @ORM\Table(
 *      name="t_cp_ville"
 * )
 * 
 * @ORM\Entity
 */
class TCpVille
{
    /**
     * @var int
     *
     * @ORM\Column(
     *      name="id_cp_town", 
     *      type="integer", 
     *      nullable=false
     * )
     * 
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Assert\GreaterThan(0)
     */
    private ?int $idCpTown = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="postal_code", 
     *      type="string", 
     *      length=5, 
     *      nullable=false, 
     *      options={
     *          "comment"="length = 5('0' <= char <= '9')"
     *      }
     * )
     * 
     * @Assert\Regex(
     * pattern="/^(?!0{2})\d{5}$/",
     * message="not_valid_postalCode"
     * )
     */
    private string $postalCode = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="town", 
     *      type="string", 
     *      length=60, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 1"
     *      }
     * )
     * 
     * @Assert\Length(
     *      min = 2,
     *      max = 60,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private string $town;

    public function getIdCpTown(): ?int
    {
        return $this->idCpTown;
    }

    public function getPostalCode(): string
    {
        return $this->postalCode;
    }

    public function setPostalCode(string $postalCode): self
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    public function __toString()
    {
        return strval($this->postalCode);
    }


    public function getTown(): string
    {
        return $this->town;
    }

    public function setTown(string $town): self
    {
        $this->town = filter_var($town, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $this;
    }
}
