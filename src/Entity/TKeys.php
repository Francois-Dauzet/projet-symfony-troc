<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TKeys
 *
 * @ORM\Table(
 *      name="t_keys", 
 *      uniqueConstraints={
 *      @ORM\UniqueConstraint(
 *          name="name_UNIQUE", 
 *          columns={"name"}
 *      )}
 * )
 * @ORM\Entity
 */
class TKeys
{
    /**
     * @var int
     *
     * @ORM\Column(
     *      name="id", 
     *      type="integer", 
     *      nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Assert\GreaterThan(0)
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="name", 
     *      type="string", 
     *      length=250, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 1, UQ"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 250,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private string $name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function __toString()
    {
        return strval($this->name);
    }

    public function setName(string $name): self
    {
        $this->name = filter_var($name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $this;
    }
}
