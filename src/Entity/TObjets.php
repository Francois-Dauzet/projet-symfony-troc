<?php

namespace App\Entity;

use App\Entity\TMessage;
use App\Entity\TCategories;
use App\Entity\TUtilisateurs;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TObjets
 *
 * @ORM\Table(name="t_objets", indexes={@ORM\Index(name="fk_user_id_idx", columns={"fk_user_id"}), @ORM\Index(name="fk_category_id_idx", columns={"fk_category_id"})})
 * @ORM\Entity
 */
class TObjets
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Assert\GreaterThan(0)
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="name", 
     *      type="string", 
     *      length=200, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 1"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 200,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private string $name = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="description", 
     *      type="text", 
     *      length=65535, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 10"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 65535,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private string $description = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="illustration", 
     *      type="string", 
     *      length=255, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 15"
     *      }
     * )
     * @Assert\Length(
     *      min = 16,
     *      max = 255,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * 
     * @Assert\Url
     */
    private string $illustration = '';

    /**
     * @var \TCategories
     *
     * @ORM\ManyToOne(targetEntity="TCategories")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(
     *          name="fk_category_id", 
     *          referencedColumnName="id"
     *      )
     * })
     * @Assert\Type("App\Entity\TCategories")
     */
    private  $fkCategory;

    /**
     * @var \TUtilisateurs
     *
     * @ORM\ManyToOne(targetEntity="TUtilisateurs")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(
     *      name="fk_user_id", 
     *      referencedColumnName="user_id"
     * )
     * })
     */
    private $fkUser;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(
     *      targetEntity="TMessage", 
     *      inversedBy="fkIdObject"
     * )
     * @ORM\JoinTable(name="t_message_objets",
     * joinColumns={
     *      @ORM\JoinColumn(
     *          name="fk_id_object", 
     *          referencedColumnName="id"
     *      )
     * },
     * inverseJoinColumns={
     *      @ORM\JoinColumn(
     *          name="fk_id_message", 
     *          referencedColumnName="id"
     *      )
     * })
     * 
     * @Assert\Type("\Doctrine\Common\Collections\Collection")
     */
    private $fkIdMessage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fkIdMessage = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = filter_var($name, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIllustration(): string
    {
        return $this->illustration;
    }

    public function setIllustration(string $illustration): self
    {
        $this->illustration = $illustration;

        return $this;
    }

    public function getFkCategory(): TCategories
    {
        return $this->fkCategory;
    }

    public function setFkCategory(TCategories $fkCategory): self
    {
        $this->fkCategory = $fkCategory;

        return $this;
    }

    public function getFkUser(): TUtilisateurs
    {
        return $this->fkUser;
    }

    public function setFkUser(TUtilisateurs $fkUser): self
    {
        $this->fkUser = $fkUser;

        return $this;
    }

    /**
     * @return Collection|TMessage[]
     */
    public function getFkIdMessage(): Collection
    {
        return $this->fkIdMessage;
    }

    public function addFkIdMessage(TMessage $fkIdMessage): self
    {
        if (!$this->fkIdMessage->contains($fkIdMessage)) {
            $this->fkIdMessage[] = $fkIdMessage;
        }

        return $this;
    }

    public function removeFkIdMessage(TMessage $fkIdMessage): self
    {
        $this->fkIdMessage->removeElement($fkIdMessage);

        return $this;
    }
}
