<?php

namespace App\Entity;

use App\Entity\TCpVille;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\TCompteUtilisateur;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * TUtilisateurs
 *
 * @ORM\Table(
 *      name="t_utilisateurs", 
 *      indexes={
 *          @ORM\Index(
 *              name="fk_cp_town_idx", 
 *              columns={"fk_cp_town_id"}
 *          )
 *      }
 * )
 * @ORM\Entity
 */
class TUtilisateurs
{

    /**
     * @var int
     *
     * @ORM\Column(
     *      name="user_id", 
     *      type="integer", 
     *      nullable=false
     * )
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Assert\GreaterThan(0)
     */
    private ?int $id = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="firstname", 
     *      type="string", 
     *      length=45, 
     *      nullable=false, 
     *      options={
     *          "comment"="> 1 char"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * 
     * @Assert\Regex(
     * pattern="/^[a-z_-çéè]{3,15}$/",
     * message="not_valid_name"
     * )
     */
    private string $firstname = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="lastname", 
     *      type="string", 
     *      length=45, 
     *      nullable=false, 
     *      options={
     *          "comment"="> 1 char"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 45,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     * 
     * @Assert\Regex(
     * pattern="/[a-z_-çéè]{3,15}$/",
     * message="not_valid_lastname"
     * )
     */
    private string $lastname = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(
     *      name="birthdate", 
     *      type="date", 
     *      nullable=false, 
     *      options={
     *          "comment"="> now - 16 years AND < now - 125 years"
     *      }
     * )
     * @Assert\Type("\DateTime")
     */
    private ?\DateTime $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="phone", 
     *      type="string", 
     *      length=10, 
     *      nullable=false, 
     *      options={
     *          "comment"="length = 10 ('0' <= char <= '9')"
     *      }
     * )
     *  @Assert\Regex(pattern="/[\+[0]]?[\+[1-7]]?[(]?[0-9]{8}[)]?[-\s\.]?$/", message="number_only")
     * 
     */
    private string $phone = '';

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="email", 
     *      type="string", 
     *      length=320, 
     *      nullable=false, 
     *      options={
     *          "comment"="Length >= 8, regex (email)"
     *      }
     * )
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email."
     * )
     */
    private string $email = '';

    /**
     * @var int
     *
     * @ORM\Column(
     *      name="street_number", 
     *      type="integer", 
     *      nullable=false, 
     *      options={"comment"="> 0"
     *      }
     * )
     * @Assert\GreaterThan(0)
     */
    private ?int $streetNumber = null;

    /**
     * @var string
     *
     * @ORM\Column(
     *      name="street_name", 
     *      type="string", 
     *      length=500, 
     *      nullable=false, 
     *      options={
     *          "comment"="length > 1"
     *      }
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 500,
     *      minMessage = "Your first name must be at least {{ limit }} characters long",
     *      maxMessage = "Your first name cannot be longer than {{ limit }} characters"
     * )
     */
    private string $streetName = '';

    /**
     * @var \TCpVille
     *
     * @ORM\ManyToOne(targetEntity="TCpVille")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(
     *      name="fk_cp_town_id", 
     *      referencedColumnName="id_cp_town"
     * )
     * })
     * @Assert\Type("App\Entity\TCpVille")
     */
    private $fkCpTown = null;

    /**
     * @var \TCompteUtilisateur
     *
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\OneToOne(targetEntity="TCompteUtilisateur")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(
     *      name="user_id", 
     *      referencedColumnName="id"
     * )
     * })
     * @Assert\Type("App\Entity\TCompteUtilisateur")
     */
    private $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = filter_var($firstname, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $this;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): self
    {
        $this->lastname = filter_var($lastname, FILTER_SANITIZE_FULL_SPECIAL_CHARS);

        return $this;
    }

    public function getBirthdate(): ?\DateTimeInterface
    {
        return $this->birthdate;
    }

    public function setBirthdate(\DateTimeInterface $birthdate): self
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getStreetNumber(): ?int
    {
        return $this->streetNumber;
    }

    public function setStreetNumber(int $streetNumber): self
    {
        $this->streetNumber = $streetNumber;

        return $this;
    }

    public function getStreetName(): string
    {
        return $this->streetName;
    }

    public function setStreetName(string $streetName): self
    {
        $this->streetName = $streetName;

        return $this;
    }

    public function getFkCpTown(): TCpVille
    {
        return $this->fkCpTown;
    }

    public function setFkCpTown(TCpVille $fkCpTown): self
    {
        $this->fkCpTown = $fkCpTown;

        return $this;
    }

    public function getUser(): TCompteUtilisateur
    {
        return $this->user;
    }

    public function setUser(TCompteUtilisateur $user): self
    {
        $this->user = $user;

        return $this;
    }
}
