<?php

namespace App\Form;

use App\Entity\TCaracteristiquesObjets;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TCaracteristiquesObjetsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fkKey')
            ->add('value')
            ->add('fkObject')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TCaracteristiquesObjets::class,
        ]);
    }
}
