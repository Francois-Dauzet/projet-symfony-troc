<?php

namespace App\Form;

use App\Entity\TUtilisateurs;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TUtilisateursType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname')
            ->add('lastname')
            //// Défini un range pour l'affichage de la date
            ->add('birthdate', DateType::class, [
                "years" => range(date('Y'), date('Y') - 125)
            ])
            ->add('phone')
            ->add('email')
            ->add('streetNumber')
            ->add('streetName')
            ->add('fkCpTown')
            ->add('user');
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TUtilisateurs::class,
        ]);
    }
}
