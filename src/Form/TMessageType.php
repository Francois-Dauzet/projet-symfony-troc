<?php

namespace App\Form;

use App\Entity\TMessage;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TMessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('objet')
            ->add('text')
            ->add('date')
            ->add('pieceJointe')
            ->add('fkEmetteur')
            ->add('fkStatut')
            ->add('fkRecepteur')
            ->add('fkIdObject')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TMessage::class,
        ]);
    }
}
